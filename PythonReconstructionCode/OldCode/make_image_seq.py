import moviepy.editor as mpy
from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import griddata
import os


############ change these parameters
degrees = 1.8  # degrees stepper motor rotates every step
microstep = 8  # is the stepper motor using microsteps? if so, what size?
step_freq = 53.3333333333333  # step frequency in hertz

subsample = 10 # save every Nth axial slice
input_data_filepath = '../phantom data/1.AVI'
save_filepath = './images_15s_53hz_phantom0' # folder to save images in
############


def run(step_freq, input_data_filepath, save_filepath, offset=0, subsample=10, degrees=1.8, microstep=8):
    if not os.path.exists(save_filepath):
        os.makedirs(save_filepath)

    vid = mpy.VideoFileClip(input_data_filepath) # load video

    rotation_time = 180/degrees*microstep/step_freq # time to rotate 180 degrees
    print(f'seconds for 180 deg: {rotation_time}')

    subvid = vid.subclip(offset, rotation_time+offset) # clip the video

    timestamps = [] # get timestamps of each frame
    for i, (tstamp, frame) in enumerate(subvid.iter_frames(with_times=True)):
        timestamps.append(tstamp)
    height = np.shape(frame)[0] # get height of the frames
    width = np.shape(frame)[1] # get width of the frames

    raxis = np.linspace(-width/2, width/2, width) # radial positions
    thetas_rad = np.array(timestamps)/rotation_time*np.pi # angular positions

    all_angles = np.zeros([height, width, len(thetas_rad)]) # add all frames into a single np array
    for i, (tstamp, frame) in enumerate(subvid.iter_frames(with_times=True)):
        all_angles[:,:,i] = np.dot(frame[...,:3], [0.299, 0.587, 0.114]) # convert to grayscale before concatenating to array

    grid_x, grid_y = np.mgrid[-width/2:width/2, -width/2:width/2] # create grid to interpolate

    for zi in range(int(height*.2), int(height*.8), subsample):
    #for zi in range(360,400,subsample):
        xy_list = np.zeros([len(thetas_rad)*width,2]) # (x, y) cartesian coordinates for all the points in an axial slice
        v_list = np.zeros(len(thetas_rad)*width) # pixel intensities (0-255) for all the points in an axial slice
        count = 0
        for ti, theta in enumerate(thetas_rad): # convert points in each axial slice into cartesian coordinates
            for ri, r in enumerate(raxis):
                x = r * np.cos(theta) #
                y = r * np.sin(theta)
                xy_list[count,:] = [x, y]
                v_list[count] = all_angles[zi, ri, ti]
                count = count+1
        v_interp = griddata(xy_list, v_list, (grid_x, grid_y), method='linear') # interpolate into a grid

        plt.figure(figsize=(width/100, width/100), dpi=150) # make sure figure resolution > interpolation grid
        plt.axis('off') # remove axes from image
        plt.imshow(v_interp, cmap='gray', vmin=0, vmax=255)
        name = f'slice{height - zi:03d}' # do (height - zi) to save images in a down to up order
        filename = os.path.join(save_filepath, name)
        plt.savefig(filename, bbox_inches='tight')
        print(f'axial slice {zi}/{height}')


#step_freqs = [40, 45, 50, 53.3333333333333, 55, 60, 65]
step_freqs = [53.3333333333333]
for freq in step_freqs:
    save_filepath = f'./images_phantom1_ss1_offset10_{int(str(freq)[:2])}'
    run(freq, '../phantom data/1.AVI', subsample=1, save_filepath=save_filepath, offset=10)

