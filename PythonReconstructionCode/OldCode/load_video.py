import numpy as np
import moviepy.editor as mpy
from matplotlib import pyplot as plt
import numpy as np
from pyevtk.hl import gridToVTK
from pathlib import Path

vid = mpy.VideoFileClip('100hz.avi') # load video

degrees = 1.8  # degrees stepper motor rotates every step
microstep = 8
step_freq = 100  # step frequency in hertz

rotation_time = 180/degrees*microstep/step_freq # time to rotate 180 degrees
print(rotation_time)

subvid = vid.subclip(0, rotation_time+0.01)  # clip video to get one revolution (the +0.01 is to get one more frame to make a full 180 degrees because Paraview can only interpolate, not extrapolate)


def write_pvd_vts(root, outputname, x, y, z, data_all_angles):
    """

    Args:
        outputname:
        x (ndarray): 3D x-coordinates
        y (ndarray): 3D y-coordinates
        z (ndarray): 3D z-coordinates
        data_all_angles (dict):  3D ndarrays of point data

    Returns:
        None
    """
    with open(root / f"{outputname}.pvd", 'w') as pvd:

        pvd.write('<?xml version="1.0"?>\n')
        pvd.write('<VTKFile type="Collection" version="0.1" '
                  'byte_order="LittleEndian" '
                  'compressor="vtkZLibDataCompressor">\n')
        pvd.write('    <Collection>\n')

        data = np.asfortranarray(data_all_angles)
        vtrfilename = root / f"{outputname}"
        pvd.write(f'        <DataSet timestep="" group="" part="muscle" file="{vtrfilename.name}.vts"/>\n')

        gridToVTK(str(vtrfilename), x, y, z, pointData={"data": data}) # save .vtk

        pvd.write('    </Collection>\n')
        pvd.write('</VTKFile>\n') # save .pvd which references .vtk


def create_3D_points(raxis, zaxis, thetas_rad):
    """create x,y,z structured grid points from raw cylindrical points

    Args:
        raxis: 1D numpy array (mm)
        zaxis: 1D numpy array (mm)
        theta_rad: angle of dataset plane (radians)

    Returns:
        x, y, z: 3D numpy arrays of Cartesian coordinates (mm) [z x r x theta]

    """
    import numpy as np

    x = np.zeros([len(zaxis), len(raxis), len(thetas_rad)])
    y = np.zeros(x.shape)
    z = np.zeros(x.shape)

    for zi, zp in enumerate(zaxis):
        for ti, tp in enumerate(thetas_rad):
            for ri, rp in enumerate(raxis):
                x[zi, ri, ti] = rp * np.cos(tp)
                y[zi, ri, ti] = rp * np.sin(tp)
                z[zi, ri, ti] = zp

    return x, y, z

timestamps = [] # get timestamps of each frame
for i, (tstamp, frame) in enumerate(subvid.iter_frames(with_times=True)):
    timestamps.append(tstamp)
height = np.shape(frame)[0] # get height of the frames
width = np.shape(frame)[1] # get width of the frames

print('found size and timestamps')

raxis = np.linspace(-width/2, width/2, width)
zaxis = np.linspace(height, 0, height)
thetas_rad = np.array(timestamps)/rotation_time*np.pi
x, y, z = create_3D_points(raxis, zaxis, thetas_rad) # convert from polar coordinates to cartesian coordinates
np.save('./x', x) # save these coordinates because coordinate transformation takes a while
np.save('./y', y)
np.save('./z', z)
# x = np.load('x.npy')
# y = np.load('y.npy')
# z = np.load('z.npy')

print('made 3D points')

all_angles = np.zeros([height, width, len(thetas_rad)]) # add all frames into a single np array
for i, (tstamp, frame) in enumerate(subvid.iter_frames(with_times=True)):
    all_angles[:,:,i] = np.dot(frame[...,:3], [0.299, 0.587, 0.114]) # convert to grayscale before concatenating to array

print('collected all angles')

root = Path('/Users/reedchen/OneDrive - Duke University/Nightingale Lab/Mammography_US/PVD_data')
outputname = '100hz_test_plus1frame_flippedpositivez'
write_pvd_vts(root, outputname, x, y, z, all_angles) # save as .pvd
print('saved')

