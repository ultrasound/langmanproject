import pydicom
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
import os

degrees = 1.8  # degrees stepper motor rotates every step
microstep = 8  # is the stepper motor using microsteps? if so, what size?
OCR1A = 468

subsample = 10
input_data_filepath = '../phantom data 2/1.dcm'
save_filepath = './images_phantom2-1-1' # folder to save images in

ds = pydicom.dcmread(input_data_filepath)
data = ds.pixel_array

if not os.path.exists(save_filepath):
    os.makedirs(save_filepath)


"""
interrupt frequency (Hz) = (Arduino clock speed 16,000,000Hz) / (prescaler * (compare match register + 1))
the +1 is in there because the compare match register is zero indexed. Divide again by 2 because it takes 2 interrupts for a single step
"""
freq = 16e6 / (256 * (OCR1A + 1)) / 2
print(freq)

height = ds.Rows  # get height of the frames
width = ds.Columns  # get width of the frames
time_vector = ds.FrameTimeVector
print(height)
print(width)

intensity_data = np.dot(data[..., :3], [0.299, 0.587, 0.114])  # convert to grayscale before concatenating to array
print(intensity_data.shape)

total_time = 0
times = []
for time in time_vector:
    total_time += time
    times.append(total_time)
print(times)
print(len(times))

angles = np.array(times) / 1000 * freq * degrees / microstep * np.pi / 180
thetas_rad = angles[angles < np.pi]
print(thetas_rad)

raxis = np.linspace(-width / 2, width / 2, width)  # radial positions
grid_x, grid_y = np.mgrid[-width / 2:width / 2, -width / 2:width / 2]  # create grid to interpolate

z_range = range(int(height * .2), int(height * .8), subsample)
for slice, zi in enumerate(z_range):
    # for zi in range(360,400,subsample):
    xy_list = np.zeros(
        [len(thetas_rad) * width, 2])  # (x, y) cartesian coordinates for all the points in an axial slice
    v_list = np.zeros(len(thetas_rad) * width)  # pixel intensities (0-255) for all the points in an axial slice
    count = 0
    for ti, theta in enumerate(thetas_rad):  # convert points in each axial slice into cartesian coordinates
        for ri, r in enumerate(raxis):
            x = r * np.cos(theta)  #
            y = r * np.sin(theta)
            xy_list[count, :] = [x, y]
            v_list[count] = intensity_data[ti, zi, ri]
            count = count + 1
    v_interp = griddata(xy_list, v_list, (grid_x, grid_y), method='linear')  # interpolate into a grid

    plt.figure(figsize=(width / 100, width / 100), dpi=150)  # make sure figure resolution > interpolation grid
    plt.axis('off')  # remove axes from image
    plt.imshow(v_interp, cmap='gray', vmin=0, vmax=255)
    name = f'slice{len(z_range) - slice:03d}'  # do (height - zi) to save images in a down to up order
    filename = os.path.join(save_filepath, name)
    plt.savefig(filename, bbox_inches='tight')
    print(f'{len(z_range) - slice}/{len(z_range)}')
